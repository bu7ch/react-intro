import logo from './logo.svg';
import './App.css';
import Tweet from './components/Tweet'
import {tweets} from './constants/index'

const App = () => (
  <div>
  <Tweet
  avatar= {tweets[0].avatar}
  username ={tweets[0].username}
  content={tweets[0].content}
  />
  <Tweet
  avatar= {tweets[1].avatar}
  username ={tweets[1].username}
  content={tweets[1].content}
  />
  </div>
  
)

export default App;
