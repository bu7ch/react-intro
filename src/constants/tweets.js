const tweets = [
  {
    avatar: 'https://picsum.photos/200/300',
    username: 'JeanMiche',
    fullname: 'Doe',
    content: 'this is content',
    date: '11/11/2020'
  },
  {
    avatar: 'https://picsum.photos/200/300',
    username: 'Billy',
    fullname: 'Doe',
    content: 'this is content',
    date: '11/11/2020'
  },
  {
    avatar: 'https://picsum.photos/200/300',
    username: 'Elliot',
    fullname: 'Doe',
    content: 'this is content',
    date: '11/11/2020'
  },
  {
    avatar: 'https://picsum.photos/200/300',
    username: 'Bob',
    fullname: 'Doe',
    content: 'this is content',
    date: '11/11/2020'
  },
  {
    avatar: 'https://',
    username: 'Wilfrid',
    fullname: 'Doe',
    content: 'this is content',
    date: '11/11/2020'
  },
];

export default tweets;