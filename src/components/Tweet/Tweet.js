import React from 'react';

const Tweet = props => (
<div>
  <div><img src={props.avatar} alt=""/></div>
  <div>
    <div>
      <strong>{props.username}</strong>
    </div>
  </div>
  <div>
    <div>
      <p>{props.content}</p>
    </div>
  </div>
</div>



)


export default Tweet
